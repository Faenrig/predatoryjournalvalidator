﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PredatoryJournalValidator.Core
{
    /// <summary>
    /// Provides the capability to read 
    /// </summary>
    public class JournalReader
    {
        public async Task<List<PredatoryJournal>> GetJournals()
        {
            string response;

            using (var client = new HttpClient())
                response = await client.GetStringAsync("https://raw.githubusercontent.com/stop-predatory-journals/stop-predatory-journals.github.io/master/_data/publishers.csv");

            var tokens = new List<string>(Regex.Split(response, ","));

            var journals = new List<PredatoryJournal>();
            int counter = 0;

            foreach (var value in tokens)
            {
                if ((counter + 1) % 3 == 0 && counter > 2)
                    journals.Add(new PredatoryJournal
                    {
                        Url = tokens[counter - 2],
                        Name = tokens[counter - 1],
                        Abbreviation = value,
                    });

                ++counter;
            }

            return journals;
        }
    }
}
