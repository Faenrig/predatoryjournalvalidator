﻿namespace PredatoryJournalValidator.Core
{
    /// <summary>
    /// A structured representation of an entry of a predatory journal.
    /// </summary>
    public class PredatoryJournal
    {
        /// <summary>
        /// The URL of a predatory journal.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// The name of a predatory journal.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The abbreviation of a predatory journal.
        /// </summary>
        public string Abbreviation { get; set; }
    }
}
