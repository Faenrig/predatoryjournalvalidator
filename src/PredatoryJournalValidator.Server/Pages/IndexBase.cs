﻿using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using PredatoryJournalValidator.Core;
using System.Threading.Tasks;
using System.Linq;

namespace PredatoryJournalValidator.Server
{
    public class IndexBase : ComponentBase
    {
        public List<PredatoryJournal> Journals { get; set; } = new List<PredatoryJournal>();

        public List<PredatoryJournal> FoundJournals { get; set; } = new List<PredatoryJournal>();

        public string SearchText { get; set; }

        private JournalReader JournalReader = new JournalReader();

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            base.OnAfterRender(firstRender);

            if (firstRender)
            {
                await GetJournalsAsync();
                FoundJournals = Journals;
            }

            await InvokeAsync(() => StateHasChanged());
        }

        public async Task GetJournalsAsync()
        {
            Journals = await JournalReader.GetJournals();
        }

        public async void SearchJournalsAsync()
        {
            if (!string.IsNullOrEmpty(SearchText))
                FoundJournals = new List<PredatoryJournal>(Journals.Where(j => j.Url.Contains(SearchText) ||
                j.Name.Contains(SearchText) ||
                j.Abbreviation.Contains(SearchText)));

            await InvokeAsync(() => StateHasChanged());
        }
    }
}
